package sheridan;

public class LoginValidator {

//Made by Vinayak for the Final Exam
    public static boolean isValidLoginName( String loginName ) {
        int count = 0;
        for (int i = 0; i < loginName.length(); i++) {
            if (!(Character.isLetter(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))) {
                return false;
            } else {
                count++;
            }
        }

        if (count < 6) {
            return false;
        }

        return true;
    }
}