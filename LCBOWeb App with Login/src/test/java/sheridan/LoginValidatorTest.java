package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

    // Test cases for the validilty of password
	
    @Test
    public void testValidLoginBoundaryIn( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Vina6574" ) );
    }

    @Test
    public void testValidLoginBoundaryOut( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Vina3768$" ) );
    }
    @Test
    public void testValidLoginRegular( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName( "PavateVinayak" ) );
    }

    @Test
    public void testValidLoginException( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
    }



//Test cases for the length of the password
    
    @Test
    public void testValidLoginNameLengthBoundaryIn( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Pava987" ) );
    }

    @Test
    public void testValidLoginNameLengthBoundaryOut( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Vina7" ) );
    }
    @Test
    public void tessValidLoginNameLengthRegular( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Vinay345" ) );
    }

    @Test
    public void testValidLoginNameLengthException( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
    }




}